/*
 * chapchap OpenCL kernel. Computes every NTLM hash that has the matching
 * last two bytes and return them to the host for additional computations.
 *
 * Copyright (c) 2012 Pierre Bourdon <delroth@lse.epita.fr>
 *
 * Some of this code comes from Cryptohaze OpenCL NTLM bruteforcer, licensed
 * under GPLv2+ (http://www.cryptohaze.com/), (c) 2011 Bitweasil.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Compute N hashes at a time. Choices: 1,2,4,8,16 */
#define VECTOR_WIDTH 8

/*
 * Enable debug logging if DEBUG is defined.
 */
// Does not work with Intel OpenCL implementation :( Uncomment if you want to
// make changes to the kernel.
#ifdef DEBUG
// # pragma OPENCL EXTENSION cl_amd_printf : enable
// # define debug_log(fmt, ...) do { \
//     printf((__constant char*)("chapchap: [DEBUG] Kernel %d: " fmt "\n"), \
//            get_global_id(0), ##__VA_ARGS__); \
// } while (0)
#else
// # define debug_log(...)
#endif

/* Define some types and macros to be able to configure vectorization easily */
#if VECTOR_WIDTH == 1
# define vector_type uint
# define ivector_type int
# define convert_vectype (uint)
# define vload_type(offset, ptr) ((ptr)[offset])
# define vstore_type(val, offset, ptr) ((ptr)[offset] = (val))

# define check_result(vec, out) \
    if ((vec) ) { uchar _off = 0; out; }
#elif VECTOR_WIDTH == 2
# define vector_type uint2
# define ivector_type int2
# define convert_vectype convert_uint2
# define vload_type vload2
# define vstore_type vstore2

# define check_result(vec, out) \
    if ((vec).s0 ) { uchar _off = 0; out; } \
    if ((vec).s1 ) { uchar _off = 1; out; }
#elif VECTOR_WIDTH == 4
# define vector_type uint4
# define ivector_type int4
# define convert_vectype convert_uint4
# define vload_type vload4
# define vstore_type vstore4

# define check_result(vec, out) \
    if ((vec).s0 ) { uchar _off = 0; out; } \
    if ((vec).s1 ) { uchar _off = 1; out; } \
    if ((vec).s2 ) { uchar _off = 2; out; } \
    if ((vec).s3 ) { uchar _off = 3; out; }
#elif VECTOR_WIDTH == 8
# define vector_type uint8
# define ivector_type int8
# define convert_vectype convert_uint8
# define vload_type vload8
# define vstore_type vstore8

# define check_result(vec, out) \
    if ((vec).s0 ) { uchar _off = 0; out; } \
    if ((vec).s1 ) { uchar _off = 1; out; } \
    if ((vec).s2 ) { uchar _off = 2; out; } \
    if ((vec).s3 ) { uchar _off = 3; out; } \
    if ((vec).s4 ) { uchar _off = 4; out; } \
    if ((vec).s5 ) { uchar _off = 5; out; } \
    if ((vec).s6 ) { uchar _off = 6; out; } \
    if ((vec).s7 ) { uchar _off = 7; out; }
#elif VECTOR_WIDTH == 16
# define vector_type uint16
# define ivector_type int16
# define convert_vectype convert_uint16
# define vload_type vload16
# define vstore_type vstore16

# define check_result(vec, out) \
    if ((vec).s0 ) { uchar _off = 0; out; } \
    if ((vec).s1 ) { uchar _off = 1; out; } \
    if ((vec).s2 ) { uchar _off = 2; out; } \
    if ((vec).s3 ) { uchar _off = 3; out; } \
    if ((vec).s4 ) { uchar _off = 4; out; } \
    if ((vec).s5 ) { uchar _off = 5; out; } \
    if ((vec).s6 ) { uchar _off = 6; out; } \
    if ((vec).s7 ) { uchar _off = 7; out; } \
    if ((vec).s8 ) { uchar _off = 8; out; } \
    if ((vec).s9 ) { uchar _off = 9; out; } \
    if ((vec).sA ) { uchar _off = 10; out; } \
    if ((vec).sB ) { uchar _off = 11; out; } \
    if ((vec).sC ) { uchar _off = 12; out; } \
    if ((vec).sD ) { uchar _off = 13; out; } \
    if ((vec).sE ) { uchar _off = 14; out; } \
    if ((vec).sF ) { uchar _off = 15; out; }
#else
# error Unsupported vector width
#endif

/* MD4 rounds definitions */
#define MD4ROTATE_LEFT(x, y) rotate((vector_type)x, (uint)y)
#define MD4F(x, y, z) bitselect((z), (y), (x))
#define MD4G(x, y, z) bitselect((y) & (z), ((z) | (y)), (x))
#define MD4H(x, y, z) ((x) ^ (y) ^ (z))
#define MD4FF(a, b, c, d, x, s) { \
    (a) += MD4F ((b), (c), (d)) + (x); \
    (a) = MD4ROTATE_LEFT ((a), (s)); \
}
#define MD4GG(a, b, c, d, x, s) { \
    (a) += MD4G ((b), (c), (d)) + (x) + (vector_type)0x5a827999; \
    (a) = MD4ROTATE_LEFT ((a), (s)); \
}
#define MD4HH(a, b, c, d, x, s) { \
    (a) += MD4H ((b), (c), (d)) + (x) + (vector_type)0x6ed9eba1; \
    (a) = MD4ROTATE_LEFT ((a), (s)); \
}

#define MD4S11 3
#define MD4S12 7
#define MD4S13 11
#define MD4S14 19
#define MD4S21 3
#define MD4S22 5
#define MD4S23 9
#define MD4S24 13
#define MD4S31 3
#define MD4S32 9
#define MD4S33 11
#define MD4S34 15

/* Full MD4 implementation */
#define MD4() { \
    a = (vector_type)0x67452301; \
    b = (vector_type)0xefcdab89; \
    c = (vector_type)0x98badcfe; \
    d = (vector_type)0x10325476; \
    MD4FF(a, b, c, d, p0, MD4S11); \
    MD4FF(d, a, b, c, p1, MD4S12); \
    MD4FF(c, d, a, b, p2, MD4S13); \
    MD4FF(b, c, d, a, p3, MD4S14); \
    MD4FF(a, b, c, d, p4, MD4S11); \
    MD4FF(d, a, b, c, (vector_type)0, MD4S12); \
    MD4FF(c, d, a, b, (vector_type)0, MD4S13); \
    MD4FF(b, c, d, a, (vector_type)0, MD4S14); \
    MD4FF(a, b, c, d, (vector_type)0, MD4S11); \
    MD4FF(d, a, b, c, (vector_type)0, MD4S12); \
    MD4FF(c, d, a, b, (vector_type)0, MD4S13); \
    MD4FF(b, c, d, a, (vector_type)0, MD4S14); \
    MD4FF(a, b, c, d, (vector_type)0, MD4S11); \
    MD4FF(d, a, b, c, (vector_type)0, MD4S12); \
    MD4FF(c, d, a, b, p14, MD4S13); \
    MD4FF(b, c, d, a, (vector_type)0, MD4S14); \
    MD4GG(a, b, c, d, p0, MD4S21); \
    MD4GG(d, a, b, c, p4, MD4S22); \
    MD4GG(c, d, a, b, (vector_type)0, MD4S23); \
    MD4GG(b, c, d, a, (vector_type)0, MD4S24); \
    MD4GG(a, b, c, d, p1, MD4S21); \
    MD4GG(d, a, b, c, (vector_type)0, MD4S22); \
    MD4GG(c, d, a, b, (vector_type)0, MD4S23); \
    MD4GG(b, c, d, a, (vector_type)0, MD4S24); \
    MD4GG(a, b, c, d, p2, MD4S21); \
    MD4GG(d, a, b, c, (vector_type)0, MD4S22); \
    MD4GG(c, d, a, b, (vector_type)0, MD4S23); \
    MD4GG(b, c, d, a, p14, MD4S24); \
    MD4GG(a, b, c, d, p3, MD4S21); \
    MD4GG(d, a, b, c, (vector_type)0, MD4S22); \
    MD4GG(c, d, a, b, (vector_type)0, MD4S23); \
    MD4GG(b, c, d, a, (vector_type)0, MD4S24); \
    MD4HH(a, b, c, d, p0, MD4S31); \
    MD4HH(d, a, b, c, (vector_type)0, MD4S32); \
    MD4HH(c, d, a, b, p4, MD4S33); \
    MD4HH(b, c, d, a, (vector_type)0, MD4S34); \
    MD4HH(a, b, c, d, p2, MD4S31); \
    MD4HH(d, a, b, c, (vector_type)0, MD4S32); \
    MD4HH(c, d, a, b, (vector_type)0, MD4S33); \
    MD4HH(b, c, d, a, p14, MD4S34); \
    MD4HH(a, b, c, d, p1, MD4S31); \
    MD4HH(d, a, b, c, (vector_type)0, MD4S32); \
    MD4HH(c, d, a, b, (vector_type)0, MD4S33); \
    MD4HH(b, c, d, a, (vector_type)0, MD4S34); \
    MD4HH(a, b, c, d, p3, MD4S31); \
    MD4HH(d, a, b, c, (vector_type)0, MD4S32); \
    d += (vector_type)0x10325476; \
}

__kernel void chapchap(__constant const uchar* restrict charset,
                       __private const uint start,
                       __private const ushort target_last2,
                       __global ushort* restrict output,
                       __global ushort* restrict output_count)
{
    int gid = get_global_id(0);
    uint out_idx = 0;

    //debug_log("start value: %016lx", start);

    /* Plaintext vectors */
    vector_type p0, p1, p2, p3, p4, p14;

    /* MD4 temporary values */
    vector_type a, b, c, d;

    /* Initialize p0-p1 from the start value */
    p0 = (vector_type)(((start >> 0) & 0xFF0000) | ((start >> 24) & 0xFF));
    p1 = (vector_type)(((start << 16) & 0xFF0000) | ((start >> 8) & 0xFF));

    /* Initialize p1-p2 from the thread id */
    uchar sc1 = charset[gid / CHARSET_SIZE / CHARSET_SIZE],
          sc2 = charset[(gid / CHARSET_SIZE) % CHARSET_SIZE],
          sc3 = charset[gid % CHARSET_SIZE];
    p1 |= (vector_type)(sc1 << 16);
    p2 = (vector_type)((sc3 << 16) | sc2);

    /* Set p14 to the plaintext size (in bits) */
    p4 = p14 = (vector_type)(PLAINTEXT_SIZE * 8 * 2);

    //debug_log("Init vector: %08x %08x %08x", p0, p1, p2);

    for (uint i = 0; i < CHARSET_SIZE; ++i)
    {
        uchar c1 = charset[i];
        for (uint j = 0; j < (CHARSET_SIZE + VECTOR_WIDTH - 1) / VECTOR_WIDTH; ++j)
        {
            vector_type c2 = convert_vectype(vload_type(j, charset));
            p3 = (vector_type)((c2 << 16) | c1);
            //debug_log("c2: %c %c %c %c", c2.s0, c2.s1, c2.s2, c2.s3);
            //debug_log("c2: %c", c2);
            //debug_log("Try: %08x %08x %08x %08x", p0, p1, p2, p3);

            MD4();

            ivector_type last_two = (d & 0xFFFF0000) == ((uint)target_last2 << 16);
            if (any(last_two))
            {
                check_result(
                    last_two,
                    output[gid + MAX_RESULTS_PER_THREAD * out_idx++]
                        = (i << 8) | (j * VECTOR_WIDTH + _off)
                );
            }
        }
    }

    output_count[gid] = out_idx;
}
