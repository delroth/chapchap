How to install chapchap
=======================

Dependencies:

* python2
* cython
* numpy
* pyopencl
* An OpenCL implementation (for CPUs, intel-opencl-sdk/amdstream, for GPUs the
  one in catalyst or cuda-toolkit)

Clone the repository, then launch `make`. You can then join a cracking cluster
by launching:

    python2 chapchap.py -s SERVER:PORT

You can stop and resume the cracking as you wish, the remote server is
intelligent enough to detect that a client stopped and redispatches the work.
