PYX=$(shell find . -name "*.pyx")
C=$(PYX:.pyx=_cython.c)
SO=$(PYX:.pyx=.so)

CC=gcc
CFLAGS=-O2 -shared -fPIC

CYTHON?=cython
PYTHON?=python2
CFLAGS+=$(shell $(PYTHON)-config --cflags)
CFLAGS+=-I/usr/lib/python2.7/site-packages/numpy/core/include

CFLAGS+=-lcrypt

all: all-so

all-so: $(SO)

%.so: %_cython.c md4.c
	$(CC) $(CFLAGS) -o $@ $< md4.c

%_cython.c: %.pyx
	$(CYTHON) -o $@ $<

clean:

distclean: clean clean-so

clean-so:
	rm -f $(SO)
