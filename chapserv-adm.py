#! /usr/bin/env python2
#
# chapserv-adm - Administrate a chapserv instance
#
# Copyright (c) 2012 Pierre Bourdon <delroth@lse.epita.fr>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import code
import xmlrpclib

BANNER = """\
Available functions:
 - add_job(CHALLENGE, RESPONSE)
 - get_completed_jobs()
 - get_jobs()
 """

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Administrate a chapserv instance"
    )
    parser.add_argument('-p', '--port', type=int, default=2132,
                        help="port used by the chapserv instance")
    parser.add_argument('-i', '--ip', type=str, default="127.0.0.1",
                        help="IP address of the chapserv instance")
    args = parser.parse_args()

    url = "http://%s:%d/" % (args.ip, args.port)
    xmlrpc = xmlrpclib.ServerProxy(url, allow_none=True)

    funcs = {
        "add_job": xmlrpc.add_job,
        "get_completed_jobs": xmlrpc.get_completed_jobs,
        "get_jobs": xmlrpc.get_jobs,
    }
    code.InteractiveConsole(funcs).interact(BANNER)
