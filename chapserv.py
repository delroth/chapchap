#! /usr/bin/env python2
#
# chapserv - Server used to distribute cracking to chapchap client
#
# Copyright (c) 2012 Pierre Bourdon <delroth@lse.epita.fr>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import chapchap_opt
import copy
import cPickle as pickle
import functools
import logging
import os.path
import SimpleXMLRPCServer as xmlrpc
import string
import threading
import time
import utils

CHARSET = string.letters + string.digits + r".,|-=&{}[]<>();$*;\/?:_^+@"
PLAINTEXT_SIZE = 8  # Do not change! Other sizes are not supported (yet?)
WORKUNIT_SIZE = len(CHARSET) ** 5
RETRY_TIMEOUT = 10 * 60  # 10 minutes
CHECK_TIMEOUT_EVERY = 60  # 1 minute

class ChapChapJob(object):
    """
    Represents a job to do
    """

    # States of a job
    NOT_STARTED, IN_PROGRESS, DONE = range(3)

    def __init__(self, jid, challenge, response, charset, plaintext_size):
        logging.debug("Creating a new job object : chall=%r, response=%r"
                        % (challenge, response))
        self.jid = jid
        self.challenge = [ord(c) for c in challenge]
        self.response = [ord(c) for c in response]
        self.charset = charset
        self.plaintext_size = plaintext_size
        self.last2 = chapchap_opt.find_last_two_bytes(challenge, response)

        self.plaintexts = []
        self.state = ChapChapJob.NOT_STARTED
        self.current_id = 0
        self.to_retry = []
        self.in_progress = []
        self.lock = threading.RLock()

        self.max_id = (len(self.charset) ** self.plaintext_size) / WORKUNIT_SIZE

        self.start_expiration_timer()

    def __getstate__(self):
        state = copy.copy(self.__dict__)
        del state["lock"]
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
        self.lock = threading.RLock()

    def start_expiration_timer(self):
        """
        Start the background thread that checks the old units expiration.
        """
        t = threading.Timer(CHECK_TIMEOUT_EVERY, self.expire_old_units)
        t.setDaemon(True)
        t.start()

    @utils.synchronized
    def get_units(self, count):
        """
        Return the IDs for at most `count` work units.
        """
        # Start by using units that are in the retry list.
        units, self.to_retry = self.to_retry[:count], self.to_retry[count:]
        remaining = count - len(units)

        if self.state == ChapChapJob.NOT_STARTED:
            self.state = ChapChapJob.IN_PROGRESS

        for i in xrange(remaining):
            a = self.current_id / len(self.charset) / len(self.charset)
            b = (self.current_id / len(self.charset)) % len(self.charset)
            c = self.current_id % len(self.charset)
            units.append((ord(self.charset[a]) << 24) |
                         (ord(self.charset[b]) << 16) |
                         (ord(self.charset[c]) << 8))
            self.current_id += 1
            if self.current_id >= self.max_id:
                break

        in_progress = [(time.time(), i) for i in units]
        self.in_progress.extend(in_progress)

        return units

    @utils.synchronized
    def handle_result(self, results):
        """
        Handle a list of results coming from a client.
        """
        for uid, result in results:
            self.plaintexts.extend(result)
            self.in_progress = [(t, i) for (t, i) in self.in_progress
                                       if i != uid]
            if uid in self.to_retry:
                self.to_retry.remove(uid)

        if self.current_id >= self.max_id and len(self.in_progress) == 0 \
                and len(self.to_retry) == 0:
            self.state = ChapChapJob.DONE

    @utils.synchronized
    def expire_old_units(self):
        """
        Called regularly to flush units that have been in progress for more
        than 30 minutes to the retry list.
        """
        logging.debug("Job %d: starting old units expiration" % self.jid)
        new_in_progress = []
        for t, uid in self.in_progress:
            if time.time() - t >= RETRY_TIMEOUT:
                logging.info("Workunit %d for job %d expired"
                                % (self.jid, uid))
                self.to_retry.append(uid)
            else:
                new_in_progress.append((t, uid))
        self.in_progress = new_in_progress
        self.start_expiration_timer()

    @property
    def progress(self):
        done_count = self.current_id - len(self.in_progress) - len(self.to_retry)
        percent = (float(done_count) / self.max_id) * 100
        cs, csl = self.charset, len(self.charset)
        c1 = cs[self.current_id / csl / csl]
        c2 = cs[(self.current_id / csl) % csl]
        c3 = cs[self.current_id % csl]
        current = c1 + c2 + c3
        return "%.03f%% (%r)" % (percent, current)

class Distributor(object):
    """
    Handles distribution of work units to clients and results aggregation.
    """

    def __init__(self):
        self.stopped = False
        self.jobs = []

    def stop(self):
        """
        Stop authorizing access to the RPC methods.
        """
        self.stopped = True

    @staticmethod
    def load(state_file):
        """
        Creates a new distributor object from the state file.
        """
        dist = pickle.load(open(state_file, "rb"))
        dist.stopped = False
        for job in dist.jobs:
            job.start_expiration_timer()
        return dist

    def save(self, state_file):
        """
        Saves the current state of the distributor to a file.
        """
        pickle.dump(self, open(state_file, "wb"))

    @utils.stoppable
    def get_workunits(self, count):
        """
        Returns at most `count` workunits not yet computed to the client.
        """
        logging.info("Worker asking for %d workunits" % count)
        for jid, job in enumerate(self.jobs):
            if job.state != ChapChapJob.DONE:
                break
        else:
            return []

        return jid, job.challenge, job.response, job.last2, job.get_units(count)

    @utils.stoppable
    def handle_result(self, jid, results):
        """
        Called by a client who finished computing work units for job `jid`.
        Results is a dict of list with keys being the work unit ids and values
        the plaintexts found in these work units (most likely none).
        """
        logging.info("Received results for job %d from a client: %s"
                        % (jid, results))
        self.jobs[jid].handle_result(results)

    @utils.stoppable
    def add_job(self, challenge, response):
        """
        Add a new challenge/response couple to the list of jobs to do. Each
        job is handled sequentially so the job won't start until the jobs
        before are completed.
        """
        logging.info("Administrator added a new job: chall=%r, resp=%r"
                        % (challenge, response))
        challenge = utils.parse_colon_string(challenge)
        response = utils.parse_colon_string(response)
        self.jobs.append(ChapChapJob(
            len(self.jobs), challenge, response, CHARSET, PLAINTEXT_SIZE
        ))

    @utils.stoppable
    def get_completed_jobs(self):
        """
        Return the list of completed jobs: (jid, chall, resp, plaintexts).
        """
        li = []
        for jid, job in enumerate(self.jobs):
            if job.state == ChapChapJob.DONE:
                li.append({
                    "id": jid,
                    "challenge": job.challenge,
                    "response": job.response,
                    "plaintexts": job.plaintexts
                })
        return li

    @utils.stoppable
    def get_jobs(self):
        """
        Return the list of jobs in the queue: (id, state, progress).
        """
        li = []
        for jid, job in enumerate(self.jobs):
            li.append({
                "id": jid,
                "state": ["not started", "in progress", "done"][job.state],
                "progress": job.progress,
                "plaintexts": job.plaintexts
            })
        return li

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Distributes the cracking work on several clients"
    )
    parser.add_argument('-q', '--quiet', action='store_true',
                        help='do not log on stdout')
    parser.add_argument('-d', '--debug', action='store_true',
                        help='display debugging informations')
    parser.add_argument('--client-port', type=int, default=2131,
                        help="port used for client connections")
    parser.add_argument('--client-listen', type=str, default="0.0.0.0",
                        help="listen for client connections on this ip")
    parser.add_argument('--admin-port', type=int, default=2132,
                        help="port used for admin connections")
    parser.add_argument('--admin-listen', type=str, default="127.0.0.1",
                        help="listen for admin connections on this ip")
    parser.add_argument('--state-file', type=str, default="chapserv.db",
                        help="file in which chapserv state is saved")
    args = parser.parse_args()

    # Set up logging
    utils.setup_logging("chapserv", args.quiet, args.debug)

    # Load the state or create a new distributor
    if os.path.exists(args.state_file):
        logging.info("Reloading the existing distributor state file")
        distributor = Distributor.load(args.state_file)
    else:
        logging.debug("No state file found - starting from scratch")
        distributor = Distributor()

    logging.debug("Creating the client XMLRPC server instance")
    client_rpc = xmlrpc.SimpleXMLRPCServer(
        (args.client_listen, args.client_port), logRequests=False,
        allow_none=True
    )
    client_rpc.register_function(distributor.get_workunits, "get_workunits")
    client_rpc.register_function(distributor.handle_result, "result")
    client_rpc_t = threading.Thread(target=client_rpc.serve_forever)
    client_rpc_t.setDaemon(True)
    client_rpc_t.start()
    logging.info("Client XMLRPC started on http://%s:%d/" % (
        args.client_listen, args.client_port
    ))

    logging.debug("Creating the admin XMLRPC server instance")
    admin_rpc = xmlrpc.SimpleXMLRPCServer(
        (args.admin_listen, args.admin_port), logRequests=False,
        allow_none=True
    )
    admin_rpc.register_function(distributor.add_job, "add_job")
    admin_rpc.register_function(distributor.get_completed_jobs,
                                "get_completed_jobs")
    admin_rpc.register_function(distributor.get_jobs, "get_jobs")
    admin_rpc_t = threading.Thread(target=admin_rpc.serve_forever)
    admin_rpc_t.setDaemon(True)
    admin_rpc_t.start()
    logging.info("Admin XMLRPC started on http://%s:%d/" % (
        args.admin_listen, args.admin_port
    ))

    # Python threads *suck*
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        pass
    finally:
        logging.info("Exit signal received, stopping properly")
        distributor.stop()
        distributor.save(args.state_file)
