# Cython module for heavier computations from chapchap.
#
# Copyright (c) 2012 Pierre Bourdon <delroth@lse.epita.fr>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

cimport cython
cimport numpy as np

# From stdlib
cdef extern from *:
    ctypedef char const_char "const char"
cdef extern void encrypt(char block[64], int edflag)
cdef extern void setkey(const_char* key)

# From md4.c
cdef extern void md4(unsigned char *from_, int from_len, unsigned char *to)

@cython.boundscheck(False)
def make_cpu_workunit(np.ndarray[np.uint16_t, ndim=1] out,
                      np.ndarray[np.uint16_t, ndim=1] out_cnt,
                      unsigned int start_val,
                      unsigned int THREADS,
                      unsigned int MAX_RESULTS_PER_THREAD):
    """
    Generates a CPU workunit from the OpenCL generated buffers.
    """
    cdef unsigned int gid, count, i, base
    cdef unsigned long long n
    block = []

    for gid from 0 <= gid < THREADS:
        count = out_cnt[gid]
        for i from 0 <= i < count:
            n = start_val
            n <<= 16
            n |= gid
            n <<= 16
            n |= out[gid + MAX_RESULTS_PER_THREAD * i]
            block.append(n)
    return block

cdef expand(unsigned char* out, unsigned char* input, size_t size):
    """
    Expand a byte string to a string with each byte == 0 or 1.
    """
    cdef int i, j

    for i from 0 <= i < size:
        for j from 8 > j >= 0:
            out[8 * i + 7 - j] = (input[i] >> j) & 1

cdef collapse(unsigned char* out, unsigned char* input, size_t size):
    """
    Reverse operation of expand: take a string with each byte == 0 or 1 and
    convert it to a byte string.
    """
    cdef int i, j
    cdef unsigned char c

    for i from 0 <= i < size:
        out[i] = 0
        for j from 8 > j >= 0:
            out[i] |= input[8 * i + 7 - j] << j

cdef des_encrypt(unsigned char* out, unsigned char* input, unsigned char* key):
    """
    Encrypts an 8 byte string using DES with a 56 bit key.
    """
    cdef unsigned char key8[8]
    cdef unsigned char key_expanded[64]
    cdef unsigned char input_expanded[64]
    cdef unsigned long long key_n
    cdef unsigned int i

    key_n = 0
    for i from 0 <= i < 7:
        key_n <<= 8
        key_n |= key[i]

    key8[0] = ((key_n >> 49) & 0x7F) << 1
    key8[1] = ((key_n >> 42) & 0x7F) << 1
    key8[2] = ((key_n >> 35) & 0x7F) << 1
    key8[3] = ((key_n >> 28) & 0x7F) << 1
    key8[4] = ((key_n >> 21) & 0x7F) << 1
    key8[5] = ((key_n >> 14) & 0x7F) << 1
    key8[6] = ((key_n >>  7) & 0x7F) << 1
    key8[7] = ((key_n >>  0) & 0x7F) << 1

    expand(key_expanded, key8, 8)
    expand(input_expanded, input, 8)

    setkey(<char*>key_expanded);
    encrypt(<char*>input_expanded, 0)
    collapse(out, input_expanded, 8)

    pass

def find_last_two_bytes(char* challenge, char* response):
    """
    Bruteforce the last two bytes of the NTLM hash from the MSCHAPv2 challenge
    and response strings.
    
    The response is made of 3 DES encrypted strings concatenated. The last one
    (characters 16 to 24) is DES(challenge, ntlm[14:16] + 5 * \0). From there
    you can compute 65536 DES hashes to find ntlm[14:16].
    """
    cdef unsigned char* chall = <unsigned char*>challenge
    cdef unsigned char* resp = <unsigned char*>response
    cdef unsigned char key[7]
    cdef unsigned char cipher[8]
    cdef unsigned short a, b, i

    key[2] = key[3] = key[4] = key[5] = key[6] = 0
    for a from 0 <= a < 0x100:
        for b from 0 <= b < 0x100:
            key[0] = a
            key[1] = b
            des_encrypt(cipher, chall, key)
            for i from 0 <= i < 8:
                if cipher[i] != resp[16 + i]:
                    break
            else:
                return (b << 8) | a

def compute_cpu_workunit(object workunit, object args,
                         char* challenge, char* response,
                         char* CHARSET, unsigned int CHARSET_SIZE):
    """
    Generate a plaintext for each number in the workunit and check if the 2 DES
    encrypted strings generated from this plaintext and the challenge match the
    response.
    """
    cdef unsigned long long n
    cdef unsigned int gid, res, i
    cdef unsigned char plain[16]
    cdef unsigned char md4_buf[16]
    cdef unsigned char des_buf[8]
    cdef unsigned char* chall = <unsigned char*>challenge
    cdef unsigned char* resp = <unsigned char*>response

    results = []

    for unit in workunit:
        n = unit

        # From the start value
        plain[0] = (n >> 56) & 0xFF
        plain[1] = 0
        plain[2] = (n >> 48) & 0xFF
        plain[3] = 0
        plain[4] = (n >> 40) & 0xFF
        plain[5] = 0

        # From the GID
        gid = (n >> 16) & 0xFFFFFF
        plain[6] = CHARSET[gid / CHARSET_SIZE / CHARSET_SIZE]
        plain[7] = 0
        plain[8] = CHARSET[(gid / CHARSET_SIZE) % CHARSET_SIZE]
        plain[9] = 0
        plain[10] = CHARSET[gid % CHARSET_SIZE]
        plain[11] = 0

        # From the thread result
        res = n & 0xFFFF
        plain[12] = CHARSET[res >> 8]
        plain[13] = 0
        plain[14] = CHARSET[res & 0xFF]
        plain[15] = 0

        # Compute the MD4
        md4(plain, 16, md4_buf)

        # Compute the first DES and check if it matches the challenge
        des_encrypt(des_buf, chall, md4_buf)
        for i from 0 <= i < 8:
            if resp[i] != des_buf[i]:
                break
        else:
            # First DES matches, try the second
            des_encrypt(des_buf, chall, md4_buf + 7)
            for i from 0 <= i < 8:
                if resp[i + 8] != des_buf[i]:
                    break
            else:
                # We found a match! Call the callback.
                results.append(''.join(c for c in plain[:16] if c != "\x00"))
    return results
