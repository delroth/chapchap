#! /usr/bin/env python2
#
# chapchap - An MSCHAPv2 password bruteforcer using OpenCL for GPU acceleration
#
# Copyright (c) 2012 Pierre Bourdon <delroth@lse.epita.fr>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import chapchap_opt
import itertools
import logging
import multiprocessing
import numpy as np
import os
import os.path
import pyopencl as cl
import socket
import string
import sys
import time
import utils
import xmlrpclib


CHARSET = string.letters + string.digits + r".,|-=&{}[]<>();$*;\/?:_^+@"
NUM_CHARSET = map(ord, CHARSET)
PLAINTEXT_SIZE = 8  # Do not change! Other sizes are not supported (yet?)
THREADS = len(CHARSET) ** 3
MAX_RESULTS_PER_THREAD = 16
WORKUNITS_AT_ONCE = 4


class OneshotWorkunitGenerator(object):
    """
    Generate sequential workunit IDs that will span the entire search space.
    """
    def __init__(self, challenge, response):
        self.challenge = utils.parse_colon_string(challenge)
        self.response = utils.parse_colon_string(response)
        if len(self.challenge) != 8 or len(self.response) != 24:
            raise ValueError("Bad challenge/response size")

        logging.debug("Bruteforcing the last two bytes of the NTLM hash")
        self.last2 = chapchap_opt.find_last_two_bytes(self.challenge,
                                                      self.response)
        if self.last2 is None:
            raise RuntimeError("Unable to find the NTLM last two bytes")
        logging.info("Last two bytes of the NTLM hash: %04X" % self.last2)

        self.iter = itertools.product(NUM_CHARSET,
                                      repeat=PLAINTEXT_SIZE - 5)
        self.jid = 0

    def start(self):
        pass

    def get_workunits(self, count):
        """
        Return a given number of workunits for the plaintexts generator.
        """
        workunits = []
        for (a, b, c) in self.iter:
            uid = (a << 24) | (b << 16) | (c << 8)
            workunits.append(uid)
            if len(workunits) == count:
                break
        if workunits == []:
            return None

        jid = self.jid
        self.jid += 1

        return jid, self.challenge, self.response, self.last2, workunits

    def send_results(self, jid, results):
        return


class RemoteWorkunitGenerator(object):
    """
    Get workunits from a remote chapserv instance. Preferred option as it
    handles stop/resume gracefully.

    NOTE: Processes are used instead of threads because PyOpenCL does not
    release the GIL properly as of 2012-07-01.
    """
    def __init__(self, server):
        logging.info("Using remote server http://%s/" % server)
        self.server = server

        self.recv_queue = multiprocessing.Queue()
        self.send_queue = multiprocessing.Queue()


    def start(self):
        """
        Starts the two network processes.
        """
        self.start_fetch_process()
        self.start_send_process()

    def start_fetch_process(self):
        """
        Starts the thread that gets workunits from the server.
        """
        def get_func():
            logging.debug("Starting the workunit fetcher process")
            rpc = xmlrpclib.ServerProxy("http://%s/" % self.server,
                                        allow_none=True)
            while True:
                logging.debug("Fetching a workunit from the server")
                try:
                    workunits = rpc.get_workunits(WORKUNITS_AT_ONCE)
                except Exception:
                    logging.info("Failed to get a workunit from the server, retrying")
                    time.sleep(1)
                    continue
                if workunits == []:
                    logging.info("No pending workunit on the server, waiting 30s")
                    time.sleep(30)
                else:
                    self.recv_queue.put(workunits, block=True)
                    while self.recv_queue.qsize() != 0:
                        time.sleep(0.5)

        p = multiprocessing.Process(target=get_func)
        p.daemon = True
        p.start()

    def start_send_process(self):
        """
        Starts the process that sends results to the server.
        """
        def send_func():
            logging.debug("Starting the workunit sender process")
            rpc = xmlrpclib.ServerProxy("http://%s/" % self.server,
                                        allow_none=True)
            while True:
                logging.debug("Waiting for workunit result")
                jid, results = self.send_queue.get()
                logging.debug("Sending workunit results to the server")
                while True:
                    try:
                        rpc.result(jid, results)
                        break
                    except Exception:
                        logging.info("Failed to send result to the server, retrying")

        p = multiprocessing.Process(target=send_func)
        p.daemon = True
        p.start()

    def get_workunits(self, count):
        """
        Returns a given number of workunits for the plaintexts generator.

        This implementaion ignores the `count` and uses a constant value.
        """
        item = self.recv_queue.get()
        return item

    def send_results(self, jid, results):
        """
        Sends results for job `jid` to the server.

        Warning: needs to be multiprocessing safe! Called from several
        subprocesses.
        """
        logging.debug("Sending results to server: %d in queue" % self.send_queue.qsize())
        self.send_queue.put((jid, results))


class PossiblePlaintextsGenerator(object):
    """
    Generate possible plaintexts from the last two bytes of the NTLM hash.
    """
    def __init__(self, wug, queue, debug=False):
        self._wug = wug
        self._queue = queue
        self._debug = debug

        # Align the charset size to 16
        self._charset = CHARSET
        while len(self._charset) % 16:
            self._charset += "\x00"

    def _init_cl_program(self):
        """
        Initializes the OpenCL runtime, loads and compiles the program with the
        correct constant definitions.
        """
        logging.debug("Creating the OpenCL context")
        self._cl_ctx = cl.create_some_context()
        self._cl_queue = cl.CommandQueue(self._cl_ctx)
        self._cl_prog = cl.Program(self._cl_ctx, self._get_cl_program_source())
        self._build_cl_program()
        self._init_cl_buffers()

    def _get_cl_program_source(self):
        """
        Locates the OpenCL source file and returns its contents.
        """
        logging.debug("Loading the OpenCL program source code")
        program_dir = os.path.dirname(sys.argv[0])
        source = os.path.join(program_dir, "chapchap.cl")
        logging.debug("OpenCL source code file: %r" % source)
        return open(source).read()

    def _build_cl_program(self):
        """
        Build the OpenCL source code providing the correct options.
        """
        defines = [
            '-DCHARSET_SIZE=%d' % len(CHARSET),
            '-DPLAINTEXT_SIZE=%d' % PLAINTEXT_SIZE,
            '-DMAX_RESULTS_PER_THREAD=%d' % MAX_RESULTS_PER_THREAD,
        ]
        if self._debug:
            defines.append('-DDEBUG')
        logging.debug("OpenCL program build flags: '%s'" % ' '.join(defines))
        logging.debug('Building...')
        self._cl_prog.build(defines)
        logging.debug('Done building the OpenCL program!')

    def _init_cl_buffers(self):
        """
        Create the OpenCL buffer objects.
        """
        logging.debug("Creating OpenCL buffers")
        self._out = np.uint16([0] * MAX_RESULTS_PER_THREAD * THREADS)
        self._out_cnt = np.uint16([0] * THREADS)

        mf = cl.mem_flags
        self._charset_buf = cl.Buffer(self._cl_ctx,
                                      mf.READ_ONLY | mf.COPY_HOST_PTR,
                                      len(self._charset), self._charset)
        self._out_buf = cl.Buffer(self._cl_ctx, mf.WRITE_ONLY,
                                  self._out.nbytes)
        self._out_cnt_buf = cl.Buffer(self._cl_ctx, mf.WRITE_ONLY,
                                      self._out_cnt.nbytes)

    def run(self):
        logging.info("Initializing the possible plaintexts generator")
        self._init_cl_program()
        logging.info("Init done, starting to generate all possible plaintexts")

        computed = 0
        curr = 0
        start_time = time.time()
        while True:
            start_get_time = time.time()
            workunits = wug.get_workunits(WORKUNITS_AT_ONCE)
            start_time += time.time() - start_get_time

            if workunits is None:
                break
            jid, challenge, response, last2, workunits = workunits

            for start in workunits:
                logging.debug("Handling workunit %d" % start)
                self.step(jid, challenge, response, last2, start)

                computed += len(CHARSET) ** 5
                curr += 1
                dt = time.time() - start_time
                if dt >= 30:
                    rate = utils.unitize(computed / dt)
                    logging.info("Current rate: %s hash/s" % rate)
                    start_time = time.time()
                    computed = 0

    def step(self, jid, challenge, response, last2, start_val):
        """
        Compute CHARSET**5 NTLM hashes starting from the provided start value.
        """
        self._cl_prog.chapchap(self._cl_queue, (THREADS,), None,
                               self._charset_buf, np.uint32(start_val),
                               np.uint16(last2), self._out_buf,
                               self._out_cnt_buf)
        e1 = cl.enqueue_read_buffer(self._cl_queue, self._out_buf,
                                    self._out)
        e2 = cl.enqueue_read_buffer(self._cl_queue, self._out_cnt_buf,
                                    self._out_cnt)
        cl.wait_for_events([e1, e2])

        workunit = chapchap_opt.make_cpu_workunit(self._out, self._out_cnt,
                                                  start_val, THREADS,
                                                  MAX_RESULTS_PER_THREAD)
        logging.debug("Queueing a new work unit")
        self._queue.put((jid, start_val, challenge, response, workunit))


def match_callback(args, plaintext):
    """
    Called when we found a match.
    """
    logging.info("Match found: %s" % plaintext)

    # Execute user specified command
    os.environ["PLAINTEXT"] = plaintext
    if args.command is not None:
        os.system(args.command)

def plaintext_handler(wug, queue, args, callback):
    """
    For each plaintext generated by the PossiblePlaintextsGenerator, check if
    the challenge can be decrypted correctly.
    """

    try:
        while True:
            logging.debug("Process %d waiting for a workunit" % os.getpid())
            item = queue.get()
            if item is None:  # used to signal process termination
                return
            jid, uid, challenge, response, workunit = item
            logging.debug("Got a workunit: size=%d (%d in queue)"
                        % (len(workunit), queue.qsize()))
            challenge = ''.join(chr(c) for c in challenge)
            response = ''.join(chr(c) for c in response)
            results = chapchap_opt.compute_cpu_workunit(workunit, args,
                                                        challenge, response,
                                                        CHARSET, len(CHARSET))
            wug.send_results(jid, [(uid, results)])
            for res in results:
                callback(args, res)
            logging.debug("Workunit done! %d in queue" % queue.qsize())
    except KeyboardInterrupt:
        logging.info("Got SIGINT, waiting for unit completion")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Crack a password transmitted using MSCHAPv2"
    )
    parser.add_argument('challenge', type=str, nargs="?", default=None,
                        help='MSCHAPv2 challenge string')
    parser.add_argument('response', type=str, nargs="?", default=None,
                        help='MSCHAPv2 response string')
    parser.add_argument('-q', '--quiet', action='store_true',
                        help='do not log on stdout')
    parser.add_argument('-d', '--debug', action='store_true',
                        help='display debugging informations')
    parser.add_argument('-s', '--server', type=str, nargs="?", default=None,
                        metavar="HOST:PORT", help="connect to this server")
    parser.add_argument('-c', '--command', type=str, nargs="?", default=None,
                        help='shell command to execute when a match is found')
    args = parser.parse_args()

    # Ugly, but at least it doesn't take 20 lines of code
    socket.setdefaulttimeout(5)

    # Set up logging
    utils.setup_logging("chapchap", args.quiet, args.debug)

    # Create the workunit generator
    if args.server is None:
        if args.challenge is None or args.response is None:
            raise ValueError("No server or challenge/response specified")
        wug = OneshotWorkunitGenerator(args.challenge, args.response)
    else:
        if args.challenge is not None or args.response is not None:
            raise ValueError("Both server and challenge/response specified")
        wug = RemoteWorkunitGenerator(args.server)

    # Start the consumer processes
    plaintexts_queue = multiprocessing.Queue(128)
    children = []
    for i in xrange(multiprocessing.cpu_count()):
        p = multiprocessing.Process(target=plaintext_handler,
                                    args=(wug, plaintexts_queue, args,
                                          match_callback))
        p.start()
        logging.info("Plaintext handler %d started (pid %d)" % (i, p.pid))
        children.append(p)

    # Start the workunit generator
    wug.start()

    # Create the feeder instance
    try:
        ppg = PossiblePlaintextsGenerator(wug, plaintexts_queue,
                                          debug=args.debug)
        ppg.run()
    except Exception:
        logging.exception("An error occurred while work was ongoing")
    finally:
        # Wait for children to complete
        logging.info("Generator exited, waiting for children")
        for i in xrange(len(children)):
            plaintexts_queue.put(None)
        for child in children:
            logging.debug("Joining child pid %d" % child.pid)
            child.join()
