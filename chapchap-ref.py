from Crypto.Cipher import DES
from Crypto.Hash import MD4

import itertools
import string
import sys

def make_8x7bit_key(key):
    n = 0
    for c in key:
        n <<= 8
        n |= ord(c)
    key = [(n >> 49) & 0x7F,
           (n >> 42) & 0x7F,
           (n >> 35) & 0x7F,
           (n >> 28) & 0x7F,
           (n >> 21) & 0x7F,
           (n >> 14) & 0x7F,
           (n >>  7) & 0x7F,
           (n >>  0) & 0x7F]
    return ''.join(chr(c << 1) for c in key)

def des_encrypt(plaintext, key):
    key = make_8x7bit_key(key)
    des = DES.new(key, DES.MODE_ECB)
    return des.encrypt(plaintext)

def parse_colon_string(s):
    bytes = s.split(':')
    return ''.join(chr(int(b, 16)) for b in bytes)

def find_last_two_bytes(challenge, response):
    last_cipher = response[16:]
    for a in xrange(256):
        for b in xrange(256):
            last_bytes = chr(a) + chr(b)
            key = last_bytes + 5 * "\x00"
            if des_encrypt(challenge, key) == last_cipher:
                return last_bytes

def all_possible_passwords(charset, size, last_two):
    for guess in itertools.product(charset, repeat=size):
        orig_guess = ''.join(guess)
        guess = '\x00'.join(guess) + '\x00'
        m = MD4.new()
        m.update(guess)
        h = m.digest()
        if h[14:16] == last_two:
            yield orig_guess, h

def bruteforce(challenge, response, charset, size):
    last_two = find_last_two_bytes(challenge, response)
    if last_two is None:
        raise RuntimeError("wtf")
    for password, hash in all_possible_passwords(charset, size, last_two):
        print("Trying %r ..." % password)
        if des_encrypt(challenge, hash[0:7]) == response[0:8]:
            if des_encrypt(challenge, hash[7:14]) == response[8:16]:
                return password

if __name__ == '__main__':
    charset = string.lowercase
    size = 6
    challenge = parse_colon_string(sys.argv[1])
    response = parse_colon_string(sys.argv[2])
    password = bruteforce(challenge, response, charset, size)
    if password is not None:
        print("match found: %r" % password)
    else:
        print("no match found")
