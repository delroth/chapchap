# Common utils used in chapchap and chapserv

import functools
import logging
import logging.handlers


def setup_logging(prog, quiet=False, debug=False):
    """
    Sets up loggers to syslog and stdout, with the correct verbosity level.
    """
    loggers = []
    loggers.append(logging.handlers.SysLogHandler('/dev/log'))
    if not quiet:
        loggers.append(logging.StreamHandler())
    for logger in loggers:
        logger.setFormatter(logging.Formatter(
            prog + ': [%(levelname)s] %(message)s'
        ))
        logging.getLogger('').addHandler(logger)
    logging.getLogger('').setLevel(
        logging.DEBUG if debug else logging.INFO
    )


class Stopped(Exception):
    """
    Exception raised when trying to run a method on a stopped distributor
    object.
    """
    pass

def stoppable(f):
    """
    Decorate a method of an object with this to make it raise a Stopped
    exception when the object is stopped.
    """
    @functools.wraps(f)
    def wrapper(self, *a, **kw):
        if self.stopped:
            raise Stopped()
        return f(self, *a, **kw)
    return wrapper


def parse_colon_string(s):
    """
    Parses a byte string delimited by colons. For example:
        c3:0f:48:95:8c:79:42:96
    """
    bytes = s.split(':')
    return ''.join(chr(int(b, 16)) for b in bytes)


def synchronized(f):
    """
    Decorator that locks an object when entering the decorated method.
    """
    @functools.wraps(f)
    def wrapper(self, *a, **kw):
        try:
            self.lock.acquire()
            return f(self, *a, **kw)
        finally:
            self.lock.release()
    return wrapper

def unitize(n):
    """
    Returns a string that represents the given number with 3 significant digits
    and an SI unit prefix. For example, 123456789 -> 123M.
    """

    units = [
        (0, ""),
        (1000, "K"),
        (1000000, "M"),
        (1000000000, "G"),
        (1000000000000, "T"),
    ]

    multiplier, prefix = None, None
    for (m, p) in units:
        if m > n:
            break
        multiplier, prefix = m, p

    if multiplier == 0:
        return str(n)

    n = float(n) / multiplier
    if n >= 100:
        format = "%d"
    elif n >= 10:
        format = "%.1f"
    else:
        format = "%.2f"
    return (format % n) + prefix
